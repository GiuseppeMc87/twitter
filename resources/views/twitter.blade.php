@extends('layouts.app')

@section('title', 'Twitter')


@section('content')

    @foreach(json_decode($twits,true) as $twit)
    <div class="row ">
        <div class="container">
            <div class="offset-md-4 col-md-4 twit">
                <div class="header">
                    <h3 class="title">
                        {{ $twit['text'] ?? '' }}
                    </h3>
                    <h6 class="referenceId">
                        Reference ID {{ $twit['id'] ?? '' }}
                    </h6>
                </div>
                <div class="twittedBy">
                    <a data-width="100%" href="{{ $twit['user']['url'] }}">Tweets by {{ $twit['user']['name'] }}</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection
