<?php

namespace Tests\Feature;

use App\Models\ProcessingRequest;
use App\Models\SocialApi;
use App\Models\SocialApi\Configurations\TwitterConfiguration;
use App\Models\SocialApi\TwitterApi;
use App\Models\String\Nonce;
use App\Models\String\TimeStamp;
use Tests\TestCase;

class TwitterAPITest extends TestCase
{
    protected $tmhOAuth;
    protected $testArray = array();

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

    }

    public function testWithRealData()
    {
        $this->testArray = [
          'credentials' => array(
              'consumer_key'    => env('TWITTER_CONSUMER_KEY'),
              'consumer_secret' => env('TWITTER_CONSUMER_SECRET'),
              'token'           => env('TWITTER_TOKEN'),
              'secret'          => env('TWITTER_SECRET'),
          ),
          'params' =>[
                array(
                    'method' => 'GET',
                    'url' => 'https://api.twitter.com/1.1/statuses/user_timeline.json',
                    'urlTmhOAuth' => '1.1/statuses/user_timeline.json',
                    'params' => [
                      'screen_name' => 'GiuseppeMuci',
                      'count'=> 1
                    ]
                ),
                array(
                    'method' => 'GET',
                    'url' => 'https://api.twitter.com/1.1/statuses/user_timeline.json',
                    'urlTmhOAuth' => '1.1/statuses/user_timeline.json',
                    'params' => [
                    'screen_name' => 'GiuseppeMuci',
                    'count'=> 1
                    ]
                ),
              ]
        ];

        $this->assertions();
        return;

    }

    public function testWithPostParams()
    {
        $this->testArray = [
            'credentials' => array(
                'consumer_key'    => env('TWITTER_CONSUMER_KEY'),
                'consumer_secret' => env('TWITTER_CONSUMER_SECRET'),
                'token'           => env('TWITTER_TOKEN'),
                'secret'          => env('TWITTER_SECRET'),
            ),
            'params' =>[
                array(
                    'method' => 'POST',
                    'url' => 'https://api.twitter.com/1.1/statuses/update.json',
                    'urlTmhOAuth' => '1.1/statuses/update.json',
                    'params' => [
                        'status' => 'hello'
                    ]
                ),
            ]
        ];

        $this->assertions();
        return;

    }



    private function assertions()
    {
        $credentials = $this->testArray['credentials'];
        foreach ($this->testArray['params'] as $arr){

            $tmhOAuth = new \tmhOAuth( $credentials );

            $tmhOAuth->user_request(array(
                'method' => $arr['method'],
                'url' => $tmhOAuth->url($arr['urlTmhOAuth']),
                'params' => $arr['params']
            ));

            $params = array(
                'method' => $arr['method'],
                'url' => $arr['url'],
                'params' => $arr['params']
            );

            $tmhOAuthResponce = $tmhOAuth->response['response'];

            $nonce = new Nonce();

            $timestamp = new TimeStamp();

            $twitterConfig = new TwitterConfiguration($credentials,$nonce,$timestamp);

            $twApi = new TwitterApi($twitterConfig);
            $twApi->setOauthOptions($params);

            $curl = new ProcessingRequest($twApi);
            $curl->oauth1Request();

            $socialApi = new SocialApi($curl);
            $this->assertJsonStringEqualsJsonString(
                $tmhOAuthResponce,
                $socialApi->getResponse()
            );
        }
    }
}
