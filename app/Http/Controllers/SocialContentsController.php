<?php

namespace App\Http\Controllers;

use App\Models\ProcessingRequest;
use App\Models\SocialApi;
use App\Models\String\Nonce;
use App\Models\String\TimeStamp;
use App\Models\SocialApi\Configurations\TwitterConfiguration;
use App\Models\SocialApi\TwitterApi;

class SocialContentsController extends Controller
{
    public function getLastTwits()
    {
        $credentials = [
            'credentials' => array(
                'consumer_key'    => env('TWITTER_CONSUMER_KEY'),
                'consumer_secret' => env('TWITTER_CONSUMER_SECRET'),
                'token'           => env('TWITTER_TOKEN'),
                'secret'          => env('TWITTER_SECRET'),
            )];

        $params = array(
            'method' => 'GET',
            'url' => 'https://api.twitter.com/1.1/statuses/user_timeline.json',
            'params' => [
                'screen_name' => 'GiuseppeMuci',
                'count'=> 3
            ]
        );


        $nonce = new Nonce();

        $timestamp = new TimeStamp();

        $twitterConfig = new TwitterConfiguration($credentials,$nonce,$timestamp);

        $twApi = new TwitterApi($twitterConfig);
        $twApi->setOauthOptions($params);

        $curl = new ProcessingRequest($twApi);
        $curl->oauth1Request();

        $socialApi = new SocialApi($curl);

        $twits = $socialApi->getResponse();

        return view('twitter', compact('twits'));
    }
}
