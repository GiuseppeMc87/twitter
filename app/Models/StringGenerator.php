<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StringGenerator extends Model
{
    protected $string;

    public function getString(){
        return $this->string;
    }
}
