<?php

namespace App\Models;

use App\Models\SocialApi\SocialApiParamsManager;
use Illuminate\Database\Eloquent\Model;

class ProcessingRequest extends Model
{
    private $socialApiIntegration;
    private $request_settings;
    private $response;

    function __construct(SocialApiParamsManager $socialApiIntegration)
    {
        $this->request_settings = $socialApiIntegration->getRequestSettings();
        $this->response = $socialApiIntegration->getResponse();
        $this->options = $socialApiIntegration->getOptions();
        $this->config = $socialApiIntegration->getConfig();
        $this->socialApiIntegration = $socialApiIntegration;
    }

    public function oauth1Request()
    {
        $this->resetRequestSettings($this->options);

        $this->setOauthParams();
        $this->prepareOauth1Params();
        $this->prepareBaseString();
        $this->prepareSigningKey();
        $this->prepareOauthSignature();
        $this->prepareAuthHeader();
        return $this->curlit();
    }

    private function resetRequestSettings($options = array())
    {

        $this->request_settings = array(
            'params' => array(),
            'headers' => array(),
//            'with_user' => true,
        );

        if (!empty($options))
            $this->request_settings = array_merge($this->request_settings, $options);
    }


    private function setOauthParams()
    {
        $this->oauthParams = array(
            'oauth_nonce' => $this->config['nonce'],
            'oauth_timestamp' => $this->config['timestamp'],
            'oauth_version' => $this->config['oauth_version'],
            'oauth_consumer_key' => $this->config['consumer_key'],
            'oauth_signature_method' => $this->config['oauth_signature_method'],
            'oauth_token' => $this->config['token']
        );
    }

    private function prepareOauth1Params()
    {
        $this->request_settings['oauth1_params'] = array();
        // safely encode
        foreach ($this->oauthParams as $k => $v) {
            $this->request_settings['oauth1_params'][$this->safe_encode($k)] = $this->safe_encode($v);
        }

        $oauth1 = &$this->request_settings['oauth1_params'];
        $params = array_merge($oauth1, $this->request_settings['params']);
        uksort($params, 'strcmp');
        foreach ($params as $k => $v) {
            $prepared_pairs_with_oauth[] = "{$this->safe_encode($k)}={$this->safe_encode($v)}";
            $prepared[$k] = $v;
            $prepared_pairs[] = "{$k}={$v}";
        }
        $this->request_settings['basestring_params'] = implode('&', $prepared_pairs_with_oauth);
        $this->request_settings['querystring'] = $prepared_pairs ? implode('&', $prepared_pairs) : null;
    }

    private function prepareBaseString()
    {
        $url = $this->request_settings['url'];
        $base = array(
            $this->request_settings['method'],
            $url,
            $this->request_settings['basestring_params']
        );
        $this->request_settings['basestring'] = implode('&', $this->safe_encode($base));
    }

    private function prepareOauthSignature()
    {
        $this->request_settings['oauth1_params']['oauth_signature'] = $this->safe_encode(
            base64_encode(
                hash_hmac(
                    'sha1', $this->request_settings['basestring'], $this->request_settings['signing_key'], true
                )));
    }

    private function prepareSigningKey()
    {
        $left = $this->safe_encode($this->config['consumer_secret']);
        $right = $this->safe_encode($this->config['secret']);
        $this->request_settings['signing_key'] = $left . '&' . $right;
    }

    private function prepareAuthHeader()
    {
        uksort($this->request_settings['oauth1_params'], 'strcmp');
        $encoded_quoted_pairs = array();
        foreach ($this->request_settings['oauth1_params'] as $k => $v) {
            $encoded_quoted_pairs[] = "{$k}=\"{$v}\"";
        }
        $header = 'OAuth ' . implode(', ', $encoded_quoted_pairs);
        $this->request_settings['headers']['Authorization'] = $header;
    }

    private function curlit()
    {
        $c = curl_init();
        if ($this->request_settings['method'] == 'GET' && isset($this->request_settings['querystring'])) {
            $this->request_settings['url'] = $this->request_settings['url'] . '?' . $this->request_settings['querystring'];
        }
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, $this->request_settings['method']);
        curl_setopt_array($c, array(
            CURLOPT_HTTP_VERSION => $this->config['curl_http_version'],
            CURLOPT_USERAGENT => $this->config['user_agent'],
            CURLOPT_CONNECTTIMEOUT => $this->config['curl_connecttimeout'],
            CURLOPT_TIMEOUT => $this->config['curl_timeout'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => $this->config['curl_ssl_verifypeer'],
            CURLOPT_SSL_VERIFYHOST => $this->config['curl_ssl_verifyhost'],

            CURLOPT_FOLLOWLOCATION => $this->config['curl_followlocation'],
            CURLOPT_URL => $this->request_settings['url'],
            CURLOPT_HEADER => false,
            CURLINFO_HEADER_OUT => true,
        ));

        if(!empty($this->request_settings['headers'])){
            foreach ($this->request_settings['headers'] as $k => $v) {
                $headers[] = trim($k . ': ' . $v);
            }
            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($c);
        $code = curl_getinfo($c, CURLINFO_HTTP_CODE);
        $info = curl_getinfo($c);
        $error = curl_error($c);
        $errno = curl_errno($c);
        curl_close($c);

        // store the response
        $this->response['code'] = $code;
        $this->response['response'] = $response;
        $this->response['info'] = $info;
        $this->response['error'] = $error;
        $this->response['errno'] = $errno;

        return $code;
    }

    private function safe_encode($data)
    {
        if (is_array($data)) {
            return array_map(array($this, 'safe_encode'), $data);
        } else if (is_scalar($data)) {
            return str_ireplace(
                array('+', '%7E'),
                array(' ', '~'),
                rawurlencode($data)
            );
        } else {
            return '';
        }
    }

    public function getResponse()
    {
        return $this->response;
    }
}
