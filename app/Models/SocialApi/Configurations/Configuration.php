<?php

namespace App\Models\SocialApi\Configurations;


use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $configuration;

    public function __construct($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getConfiguration()
    {
        return $this->configuration;
    }
}
