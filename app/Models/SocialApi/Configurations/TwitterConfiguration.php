<?php

namespace App\Models\SocialApi\Configurations;


class TwitterConfiguration extends Configuration
{
    public function __construct($config = array(),$nonce,$timestamp)
    {
        parent::__construct($config);

        $this->configuration = array_merge(
            array(
                'user_agent'                 => 'Twitter API GM 0.01',
                'method'                     => 'GET',
                'consumer_key'               => env('TWITTER_CONSUMER_KEY'),
                'consumer_secret'            => env('TWITTER_CONSUMER_SECRET'),
                'token'                      => env('TWITTER_TOKEN'),
                'secret'                     => env('TWITTER_SECRET'),
                'oauth_version'              => '1.0',
                'oauth_signature_method'     => 'HMAC-SHA1',

                'curl_http_version'          => CURL_HTTP_VERSION_1_1,
                'curl_connecttimeout'        => 30,
                'curl_timeout'               => 10,
                'curl_ssl_verifyhost'        => 2,
                'curl_ssl_verifypeer'        => true,
                'curl_sslversion'            => false,
                'curl_followlocation'        => false,
                'nonce' => $nonce->getString(),
                'timestamp' => $timestamp->getString()
            ),
            $config
        );
    }
}
