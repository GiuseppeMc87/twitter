<?php

namespace App\Models\SocialApi;

use App\Models\ProcessingRequest;
use App\Models\SocialApi\Configurations\Configuration;
use App\Models\StringGenerator;

class TwitterApi extends SocialApiParamsManager
{
    /**
     * Creates a new tmhOAuth object
     *
     * @param StringGenerator $config , the configuration to use for this request
     * @return void
     */
    public function __construct(Configuration $configuration)
    {
        $this->buffer = null;
        $this->config = $configuration->getConfiguration();
    }

}
