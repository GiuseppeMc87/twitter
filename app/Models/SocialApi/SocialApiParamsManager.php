<?php

namespace App\Models\SocialApi;

use Illuminate\Database\Eloquent\Model;

class SocialApiParamsManager extends Model
{
    private $response = array();
    private $request_settings = array();
    protected $config;
    protected $buffer;
    protected $options = array();

    public function setOauthOptions ($options = array())
    {
        $this->options = $options;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getRequestSettings()
    {
        return $this->request_settings;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getConfig()
    {
        return $this->config;
    }
}
