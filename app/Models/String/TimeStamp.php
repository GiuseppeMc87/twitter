<?php

namespace App\Models\String;

use App\Models\StringGenerator;

class TimeStamp extends StringGenerator
{
    public function __construct($time = null)
    {
        if(!$time){
            $time = time();
        }
        $this->string = $time;
    }
}
