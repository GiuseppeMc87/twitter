<?php

namespace App\Models\String;

use App\Models\StringGenerator;

class Nonce extends StringGenerator
{
    public function __construct($nonce = null )
    {
        if($nonce){
            $this->string = $nonce;
        }else{
            $randValue = time() . microtime() . uniqid();
            $this->string = md5($randValue);
        }
    }
}
