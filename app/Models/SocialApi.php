<?php

namespace App\Models;

use App\Models\SocialApi\TwitterConfiguration;
use Illuminate\Database\Eloquent\Model;

class SocialApi extends Model
{

    protected $response = array();
    protected $curl;

    public function __construct(ProcessingRequest $curl)
    {
        $this->curl = $curl;
        $curlResponse = $this->curl->getResponse();
        $this->response = $curlResponse['response'];
    }

    public function getResponse()
    {
        return $this->response;
    }
}
